# قوانين خادم سبادون رول بلاي ![](https://readthedocs.org/projects/spadone/badge/?version=latest)

قوانين لعب الإدوار الخاصة بسبادون رول بلاي، يُمكن قراءتها من خلال: https://docs.spadone.net أو https://spadone.readthedocs.io

نستقبل جميع الإقتراحات والمساعدات لكتابة وتهيئة هذه القوانين.